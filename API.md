# InfinityTable API
This document is intended to explain, and plan, the api endpoints for the InfinityTable Tabletop Roleplay System. Going forward it will serve as the contract between front-end clients and the backend system that will perform data persistence, and well as a large portion of the business logic.

Endpoints for the API are devided into groups known as apps. These include [Campaigns](#campains), [Games](#games), [Groups](#groups), and [Users](#users).

Each app will have a main entity endpoint, and defined entity enpoint. This document will list each method available on each endpoint, requirements,and high level arguments available for each.

Communication with the API will be through json packets, (mostly) following the [JSONAPI](http://jsonapi.org) spec. Endpoints will be attempting to hold to REST priciples, allowing for some deviation where it makes sense for simplicty.

Endpoints can de described into 2 groups:
* Collections
  - Returns an array on _GET_
  - Adds an Item on _POST_
* Items
  - Fetch details on _GET_
  - Update an item on _PUT_
  - Remove an item on _DELETE_

These are generalizations, and should be confirmed within the documentation.

For the purposes of this document we will be assuming the url of the API will be `https://inifinitytable.com/api`

## Campaigns
Campaigns are a string of [Games](#games) that comprise one storyline. Typically a campaign will be run by one storteller, and have the same group of players throughout.
* Path: /campaigns
* __GET__ List of campaigns
  - Calling a list of campaigns accepting new players with a pagination length of 10 could look like this: `https://intinitytable.com/api/campaigns?status=open&limit=10`
  - This would return a JSON objects like this:

    ```json
    {
      "links": {
        "self": "https://intinitytable.com/api/campaigns?status=open&limit=10",
        "next": "https://intinitytable.com/api/campaigns?status=open&limit=10&start=10",
        "previous": ""
      },
      "data": [
        {
          "id": 1,
          "type": "campaign",
          "data": {
            "id": 1,
            "title": "The Deep Dark Dungeon",
            "storyteller": {
              "id": 10,
              "username": "StoryMaster",
              "url": "https://intinitytable.com/api/users/10"
            },
            "players": [
              {
                "id": 2,
                "username": "Newbie",
                "url": "https://intinitytable.com/api/users/2"
              },
              {
                "id":1,
                "username": "TheVeteran",
                "url": "https://intinitytable.com/api/users/1"
              }
            ],
            "start_date": "2016-12-01",
            "system": "WhiteWolf: 2ndGen"
          }
        }
      ],
      "success": true,
      "errors": []
    }
    ```
  - If an error was to occur, the response would look more like this:

    ```json
    {
      "links": {
        "self": "https://intinitytable.com/api/campaign?status=open&limit=10",
        "next": "",
        "previous": ""
      },
      "data": [],
      "success": false,
      "errors": [
        {
          "code": 404,
          "message": "Collection Type not found"
        }
      ]
    }
    ```
    + This will also include an http status of 404 (or the applicable status code)
* __POST__ Add a campaign
  - To add an item, post a json object into the request body including the following fields:
    + title

    ```json
    {
      "title": "Steampunk Duneon Crawler",
      "description": "Victorian monster hunting!"
    }
    ```
    + The response, if successful, will include the newly created object

    ```json
    {
      "links": {
        "self": "https://intinitytable.com/api/campaigns/1234",
        "next": "",
        "previous": ""
      },
      "data": {
        "id": 1234,
        "title": "Steampunk Dungeon Crawler",
        "description": "Victorian monster hunting!",
        "created_date": "2016-12-08",
        "created_by": {
          "id": 10,
          "username": "StoryMaster",
          "url": "https://intinitytable.com/api/users/10"
        }
      },
      "success": true,
      "errors": []
    }
    ```

* ### Campaign Item
  - Path: /campaigns/{campaign id}
  - __GET__ Fetch details for the specified campaign
    + Calling `https://infinitytable.com/api/campaigns/1234` would yield the result

    ```json
    {
      "links": {
        "self": "https://intinitytable.com/api/campaigns/1234",
        "next": "",
        "previous": ""
      },
      "data": {
        "id": 1234,
        "title": "Steampunk Dungeon Crawler",
        "description": "Victorian monster hunting!",
        "created_date": "2016-12-08",
        "created_by": {
          "id": 10,
          "username": "StoryMaster",
          "url": "https://intinitytable.com/api/users/10"
        }
      },
      "success": true,
      "errors": []
    }
    ```
  - __PUT__ Update a campaign
    + push the details of the campaign within the request body, just as with the __POST__ call used to add a new item:
      `https://infinitytable.com/api/campaigns/1234` with the body

    ```json
    {
      "title": "Steampunk Dungeon Crawler",
      "description": "Victorian monster hunting!"
    }
    ```

    Will yield:

    ```json
    {
      "links": {
        "self": "https://intinitytable.com/api/campaigns/1234",
        "next": "",
        "previous": ""
      },
      "data": {
        "id": 1234,
        "title": "Steampunk Dungeon Crawler",
        "description": "Victorian monster hunting!",
        "created_date": "2016-12-08",
        "created_by": {
          "id": 10,
          "username": "StoryMaster",
          "url": "https://intinitytable.com/api/users/10"
        }
      },
      "success": true,
      "errors": []
    }
    ```

# Games

Endpoint used for listing, starting, and updating game sessions.



# Groups

# Users