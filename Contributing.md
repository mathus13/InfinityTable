#Contributing#

This project is currently planned to use a PHP powered JSONAPI complient backend
Using MySQL for a persistance layer. (this may change)

The front end is speced to use Vue. It is build right now with Vue v1, but may be updating shortly to Vue v2, or the front end may be spun off to its own Repo.
 
This is intended to keep the project very API focused (and because we can't decide of a front end). If you are interested in becoming a contributer, please message me with a desired module you want to work on, and fork away. Also, tests are always apreciated!

\- Shawn