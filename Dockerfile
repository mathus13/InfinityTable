FROM php:latest
COPY . /usr/src/infinity
WORKDIR /usr/src/infinity/public/api
EXPOSE 8000
CMD php -S 0.0.0.0:8000 index.php