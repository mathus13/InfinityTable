<?php
namespace Infinity;

class Utilities
{
    static function loadHookFiles(Ethereal\SimpleHooks $hookSystem)
    {
        $hooks = array();
        foreach (\DirectoryIterator('.') as $dir) {
            if (strpos('hooks.json', $dir->getFilename()) !== false) {
                $data = file_get_contents($dir->getPathname());
                if (!$data) {
                	continue;
                }
                $listeners = json_decode($data);
                foreach ($listeners as $topic => $listener) {
                	$hookSystem->addListener($topic, $listener->class, $listener->method);
                }
            }
        }
    }
}
