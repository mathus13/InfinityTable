<?php
namespace Infinity;

class MetaTable extends Ethereal\Db\MetaTable implements \Ethereal\Db\TableInterface
{
    public function getAllActive()
    {
        return $this->fetchAll(
            "SELECT main.*, GROUP_CONCAT(md_name,'::',md_value separator '||') AS meta_data 
            FROM {$this->table} main LEFT JOIN {$this->meta_table} md  ON ref_id = client.id 
            WHERE `active` = 1 GROUP BY client.id ORDER BY created_date"
        );
    }
}
